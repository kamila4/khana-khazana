package Day4;
import java.util.ArrayList;

public class TesterOrder {
	

	
	
		//{new Item("Pasta",300),new Item("Brownie",120),new Item("Pizza",399)};
	
	
	//static OrderItem[] order = {new OrderItem(allItems[0],2),new OrderItem(allItems[1],1)};

	public static void main(String[] args) {
		ArrayList<Item> allItems= new ArrayList<Item>();
		allItems.add(new Item("Fries",99));
		allItems.add(new Item("Pasta",300));
		allItems.add(new Item("Brownie",120));
		allItems.add(new Item("Pizza",399));
		
		ArrayList<OrderItem> order= new ArrayList<OrderItem>();
		order.add(new OrderItem(allItems.get(0),2));
		order.add(new OrderItem(allItems.get(1),1));
		order.add(new OrderItem(allItems.get(2),1));
		
		
		
		
		Order order1 = new Order(order);
		order1.calculateTotal();
		System.out.println("Total Price of Order id "+order1.orderId+" is :Rs."+order1.totalPrice+ " with status: "+order1.status);
		order1.updateStatus("Confirmed");
		order1.orderDetails();
		

	}

}
