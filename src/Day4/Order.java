package Day4;

import java.util.ArrayList;

public class Order {
	static int oid = 0;
	int orderId;
	OrderItem[] items;
	String status;
	double totalPrice;
	
	Order(ArrayList<OrderItem> order){
		orderId = ++oid;
		status = "Pending";
		OrderItem[] temp = new OrderItem[order.size()];
		items =  order.toArray(temp);
	}
	
	
	void calculateTotal() {
		
		double sum =0.0;
		for(OrderItem i:items) {
			sum += i.item.pricePerUnit * i.quantity;
		}
		totalPrice = sum;
		
	}
	
	void checkStatus() {
		System.out.println(status);
	}
	
	void updateStatus(String s) {
		status = s;
		System.out.println("Order status changed to : "+status);
	}
	
	void orderDetails() {
		System.out.println("\nOrder details for order no : "+orderId+" ");
		System.out.println("----------------------");
		for(OrderItem i:items) {
			System.out.println(i.item.itemName+" x "+i.quantity+" : Rs."+i.item.pricePerUnit*i.quantity);
		}
		System.out.println("\n----------------------\nTotal Amount = Rs."+totalPrice);
	}
}
