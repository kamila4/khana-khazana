
public class Program2 extends Thread {
	static int amount = 0;
	public static void main(String[] args) {
		
		
		
		Program2 thread = new Program2();
		
		System.out.println("Main: "+ amount);
		amount++;
		thread.start();
		while(thread.isAlive()) {
			System.out.println("Waiting...");
		}
		System.out.println("Main: "+ amount);
	}
	public void run() {
		amount++;
	}
}
